package com.quickloan.app.service;

import static java.math.BigDecimal.ZERO;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quickloan.app.configuration.Properties;
import com.quickloan.app.model.Loan;
import com.quickloan.app.repository.LoanRepository;
import com.quickloan.app.repository.NotFoundLoanException;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class LoanServiceTest {

  @Mock
  private LoanRepository loanRepository;
  @Mock
  private RepaymentService repaymentService;
  @Mock
  private Properties properties;
  private LoanService underTest;

  @BeforeEach
  private void init() {
    underTest = new LoanService(loanRepository, repaymentService, properties);
    Mockito.reset(properties, loanRepository);
  }

  @Test
  void applyForLoanPositive() {
    Loan loan = new Loan(1, 1, 1, ZERO, LocalDate.now());
    loan.setId(1L);
    when(loanRepository.save(any(Loan.class))).thenReturn(loan);

    Loan result = underTest.createLoan(1, 1);

    assertNotNull(result);
    assertNotNull(result.getId());
    verify(loanRepository).save(any(Loan.class));
  }

  @Test
  void extendLoanPositive() {
    Loan loan = new Loan(1, 1, 1, ZERO, LocalDate.now());
    when(loanRepository.findById(1L)).thenReturn(Optional.of(loan));
    when(loanRepository.save(any(Loan.class))).thenReturn(loan);
    underTest.extendLoan(1);
  }

  @Test
  void extendLoanNegative() {
    assertThrows(NotFoundLoanException.class, () ->underTest.extendLoan(1));
  }
}
