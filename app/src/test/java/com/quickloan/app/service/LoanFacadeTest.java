package com.quickloan.app.service;

import static com.quickloan.app.api.LoanApplicationResult.Status.SUCCESS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quickloan.app.api.LoanApplicationResult;
import com.quickloan.app.api.LoanExtendDto;
import com.quickloan.app.api.LoanExtendResult;
import com.quickloan.app.model.Loan;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class LoanFacadeTest {

  @Mock
  private LoanValidationService loanValidationService;
  @Mock
  private LoanService loanService;
  private LoanFacade underTest;

  @BeforeEach
  private void init() {
    underTest = new LoanFacade(loanValidationService, loanService);
  }

  @Test
  void happyPath_applyForLoan() {
    BigDecimal expectedAmount = BigDecimal.valueOf(1.34);
    LocalDate expectedDate = LocalDate.of(2222,1,1);
    Long expectedId = 1L;
    Loan expectedLoan = new Loan(1, 1, 1, expectedAmount, expectedDate);
    ReflectionTestUtils.setField(expectedLoan, "id", 1L);

    when(loanService.createLoan(1, 1)).thenReturn(expectedLoan);

    LoanApplicationResult loanApplicationResult = underTest.createLoan(1, 1);

    verify(loanService).createLoan(1, 1);
    assertEquals(SUCCESS, loanApplicationResult.status);
    assertEquals(expectedId, loanApplicationResult.applicationId);
    assertEquals(expectedAmount, loanApplicationResult.repaymentAmount);
    assertEquals(expectedDate, loanApplicationResult.repaymentDate);
  }

  @Test
  void happyPath_extendLoan() {
    when(loanValidationService.loanExists(any(LoanExtendDto.class))).thenReturn(true);

    LoanExtendResult loanExtendResult = underTest.extendLoan(new LoanExtendDto(1));

    verify(loanService).extendLoan(1);
    assertEquals(LoanExtendResult.Status.SUCCESS, loanExtendResult.status);
    assertEquals("", loanExtendResult.description);
  }

  @Test
  void loanNotExists_extendLoan() {
    when(loanValidationService.loanExists(any(LoanExtendDto.class))).thenReturn(false);

    LoanExtendResult loanExtendResult = underTest.extendLoan(new LoanExtendDto(1));

    verify(loanService, never()).extendLoan(1);
    assertEquals(LoanExtendResult.Status.FAILED, loanExtendResult.status);
    assertNotNull(loanExtendResult.description);
  }
}