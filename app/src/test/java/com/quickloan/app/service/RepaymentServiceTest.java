package com.quickloan.app.service;

import static java.math.RoundingMode.HALF_UP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RepaymentServiceTest {

  @Mock
  private TimeService timeService;
  private RepaymentService underTest;

  @BeforeEach
  void init() {
    underTest = new RepaymentService(timeService);
  }

  @Test
  void calculateAmount() {
    BigDecimal expected = BigDecimal.valueOf(111.1).setScale(2, HALF_UP);

    BigDecimal amount = underTest.calculateAmount(101, 10);

    assertEquals(expected, amount);
  }

  @Test
  void calculateDate() {
    LocalDate createdDate = LocalDate.of(2019,2,1);
    int daysToAdd = 20;
    when(timeService.now()).thenReturn(LocalDateTime.of(createdDate, LocalTime.now()));

    LocalDate date = underTest.calculateDueDate(daysToAdd);
    assertEquals(createdDate.plusDays(daysToAdd), date);
  }
}