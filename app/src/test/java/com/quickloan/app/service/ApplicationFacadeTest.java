package com.quickloan.app.service;

import static com.quickloan.app.api.LoanApplicationResult.Status.REJECTED;
import static com.quickloan.app.api.LoanApplicationResult.Status.SUCCESS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.quickloan.app.api.LoanApplicationResult;
import com.quickloan.app.api.LoanApplyDto;
import com.quickloan.app.model.Application;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ApplicationFacadeTest {

  @Mock
  private ApplicationService applicationService;
  @Mock
  private LoanFacade loanFacade;
  @Mock
  private LoanValidationService validationService;
  private ApplicationFacade underTest;

  @BeforeEach
  void init() {
    Mockito.reset(applicationService, loanFacade, validationService);
    underTest = new ApplicationFacade(applicationService, loanFacade, validationService);
  }

  @Test
  void applyForLoan_happyPath() {
    LocalDate expectedDate = LocalDate.of(2222, 1, 1);
    BigDecimal expectedAmount = BigDecimal.valueOf(133);
    Long expectedId = 1L;
    when(applicationService.saveApplication(anyInt(), anyInt()))
        .thenReturn(new Application(100, 31));
    when(validationService.isValidInput(any(LoanApplyDto.class))).thenReturn(true);
    when(loanFacade.createLoan(anyInt(), anyInt()))
        .thenReturn(LoanApplicationResult.success(expectedAmount, expectedDate,
            expectedId));

    LoanApplyDto dto = new LoanApplyDto(1, 1);
    LoanApplicationResult result = underTest.applyForLoan(dto);

    assertEquals(expectedId, result.applicationId);
    assertEquals(expectedAmount, result.repaymentAmount);
    assertEquals(expectedDate, result.repaymentDate);
    assertEquals(SUCCESS, result.status);
  }

  @Test
  void applyForLoan_invalidInput() {
    when(applicationService.saveApplication(anyInt(), anyInt()))
        .thenReturn(new Application(100, 31));
    when(validationService.isValidInput(any(LoanApplyDto.class))).thenReturn(false);

    LoanApplyDto dto = new LoanApplyDto(1, 1);
    LoanApplicationResult result = underTest.applyForLoan(dto);

    assertNull(result.applicationId);
    assertNull(result.repaymentAmount);
    assertNull(result.repaymentDate);
    assertEquals(REJECTED, result.status);
  }
}