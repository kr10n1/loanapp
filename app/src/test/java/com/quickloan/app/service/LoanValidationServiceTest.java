package com.quickloan.app.service;

import static java.math.BigDecimal.ZERO;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.quickloan.app.api.LoanApplyDto;
import com.quickloan.app.api.LoanExtendDto;
import com.quickloan.app.configuration.Properties;
import com.quickloan.app.model.Loan;
import com.quickloan.app.repository.LoanRepository;
import com.quickloan.app.service.LoanValidationServiceTest.LocalConfiguration;
import com.quickloan.app.service.validation.LoanExists;
import com.quickloan.app.service.validation.ValidAfterHours;
import com.quickloan.app.service.validation.ValidPrincipal;
import com.quickloan.app.service.validation.ValidTerm;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = {LocalConfiguration.class})
@ExtendWith(SpringExtension.class)
class LoanValidationServiceTest {

  private static final int MIN_PRINCIPAL = 1;
  private static final int MAX_PRINCIPAL = 10;
  private static final int VALID_PRINCIPAL = 4;
  private static final int VALID_TERM = 4;
  private static final int LESS_THAN_MIN_TERM = 0;
  private static final int MORE_THAN_MAX_TERM = 11;
  private static final int LESS_THAN_MIN_AMOUNT = 0;
  private static final int MORE_THAN_MAX_AMOUNT = 11;
  private static final DateTimeFormatter FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
  private static final LocalDateTime IN_HOURS = LocalDateTime.parse("2010-10-10 10:10", FORMATTER);
  private static final LocalDateTime AFTER_HOURS =
      LocalDateTime.parse("2001-01-01 01:01", FORMATTER);
  private static final LocalTime OPEN_TIME = LocalTime.parse("06:00:00");
  private static final LocalTime CLOSE_TIME = LocalTime.parse("00:00:00");

  @Autowired
  private Properties propertiesMock;
  @Autowired
  private LoanRepository loanRepositoryMock;
  @Autowired
  private TimeService timeServiceMock;

  @Autowired
  private LoanValidationService underTest;

  @BeforeEach
  void init() {

  }

  @Test
  void validTermAndPrincipal() {
    when(timeServiceMock.now()).thenReturn(IN_HOURS);
    LoanApplyDto dto = new LoanApplyDto(VALID_TERM, VALID_PRINCIPAL);

    boolean valid = underTest.isValidInput(dto);

    assertTrue(valid);
  }

  @Test
  void invalidMinTerm() {
    testTermWith(LESS_THAN_MIN_TERM);
  }

  @Test
  void invalidMaxTerm() {
    testTermWith(MORE_THAN_MAX_TERM);
  }

  private void testTermWith(int term) {
    when(timeServiceMock.now()).thenReturn(IN_HOURS);
    LoanApplyDto dto = new LoanApplyDto(term, VALID_PRINCIPAL);

    boolean valid = underTest.isValidInput(dto);

    assertFalse(valid);
  }

  @Test
  void invalidMinAmount() {
    testAmountWith(LESS_THAN_MIN_AMOUNT);
  }

  @Test
  void invalidMaxAmount() {
    testAmountWith(MORE_THAN_MAX_AMOUNT);
  }

  private void testAmountWith(int principal) {
    when(timeServiceMock.now()).thenReturn(IN_HOURS);
    LoanApplyDto dto = new LoanApplyDto(VALID_TERM, principal);

    boolean valid = underTest.isValidInput(dto);

    assertFalse(valid);
  }

  @Test
  void validAmountAfterHours() {
    when(timeServiceMock.now()).thenReturn(AFTER_HOURS);
    LoanApplyDto dto = new LoanApplyDto(VALID_TERM, MAX_PRINCIPAL - 1);

    boolean valid = underTest.isValidInput(dto);

    assertTrue(valid);
  }

  @Test
  void invalidAmountAfterHours() {
    when(timeServiceMock.now()).thenReturn(AFTER_HOURS);
    LoanApplyDto dto = new LoanApplyDto(VALID_TERM, MAX_PRINCIPAL);

    boolean valid = underTest.isValidInput(dto);

    assertFalse(valid);
  }

  @Test
  void loanExists() {
    when(loanRepositoryMock.findById(any()))
        .thenReturn(Optional.of(new Loan(1, 1, 1, ZERO, LocalDate.now())));

    boolean exists = underTest.loanExists(new LoanExtendDto(1));

    assertTrue(exists);
  }

  @Test
  void loanNotExists() {
    when(loanRepositoryMock.findById(any())).thenReturn(Optional.empty());

    boolean exists = underTest.loanExists(new LoanExtendDto(1));

    assertFalse(exists);
  }

  @Configuration
  static class LocalConfiguration {

    @Bean
    LoanValidationService validationService(ValidAfterHours validAfterHours,
        ValidPrincipal validPrincipal, ValidTerm validTerm, LoanExists loanExists) {
      return new LoanValidationService(validAfterHours, validPrincipal, validTerm, loanExists);
    }

    @Bean
    ValidAfterHours validAfterHours(TimeService timeService, Properties properties) {
      return new ValidAfterHours(timeService, properties);
    }

    @Bean
    ValidPrincipal validPrincipal(Properties properties) {
      return new ValidPrincipal(properties);
    }

    @Bean
    ValidTerm validTerm(Properties properties) {
      return new ValidTerm(properties);
    }

    @Bean
    LoanExists loanExists(LoanRepository loanRepository) {
      return new LoanExists(loanRepository);
    }

    @Bean
    Properties propertiesMock() {
      Properties properties = new Properties();
      properties.setMinAmount(MIN_PRINCIPAL);
      properties.setMaxAmount(MAX_PRINCIPAL);
      properties.setMinTerm(MIN_PRINCIPAL);
      properties.setMaxTerm(MAX_PRINCIPAL);
      properties.setOpenTime(OPEN_TIME.toString());
      properties.setCloseTime(CLOSE_TIME.toString());
      return properties;
    }

    @Bean
    LoanRepository loanRepositoryMock() {
      return mock(LoanRepository.class);
    }

    @Bean
    TimeService timeServiceMock() {
      return mock(TimeService.class);
    }
  }
}

