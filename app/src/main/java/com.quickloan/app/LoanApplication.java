package com.quickloan.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.java.Log;
@SpringBootApplication(scanBasePackages = "com.quickloan.app")
@Log
public class LoanApplication {

  public static void main(String args[]) {
    log.info("starting application");
    SpringApplication.run(LoanApplication.class, args);
  }
}
