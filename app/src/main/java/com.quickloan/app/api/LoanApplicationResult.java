package com.quickloan.app.api;

import static com.quickloan.app.api.LoanApplicationResult.Status.REJECTED;
import static com.quickloan.app.api.LoanApplicationResult.Status.SUCCESS;

import java.math.BigDecimal;
import java.time.LocalDate;

public class LoanApplicationResult {

  public final BigDecimal repaymentAmount;
  public final LocalDate repaymentDate;
  public final Long applicationId;
  public final Status status;

  private LoanApplicationResult(BigDecimal repaymentAmount, LocalDate repaymentDate, Long applicationId,
      Status status) {
    this.repaymentAmount = repaymentAmount;
    this.repaymentDate = repaymentDate;
    this.applicationId = applicationId;
    this.status = status;
  }

  public enum Status {
    REJECTED,
    SUCCESS
  }

  public static LoanApplicationResult success(BigDecimal repaymentAmount, LocalDate repaymentDate, Long applicationId) {
    return new LoanApplicationResult(repaymentAmount, repaymentDate, applicationId, SUCCESS);
  }

  public static LoanApplicationResult rejected() {
    return new LoanApplicationResult(null, null, null, REJECTED);
  }
}
