package com.quickloan.app.api;

import static com.quickloan.app.api.LoanExtendResult.Status.FAILED;
import static com.quickloan.app.api.LoanExtendResult.Status.SUCCESS;

public class LoanExtendResult {

  public final Status status;
  public final String description;

  private LoanExtendResult(Status status, String description) {
    this.status = status;
    this.description = description;
  }

  public static LoanExtendResult success() {
    return new LoanExtendResult(SUCCESS, "");
  }

  public static LoanExtendResult failed(String description) {
    return new LoanExtendResult(FAILED, description);
  }

  public enum Status {
    SUCCESS,
    FAILED
  }
}
