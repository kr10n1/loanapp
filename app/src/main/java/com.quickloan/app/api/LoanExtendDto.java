package com.quickloan.app.api;

import lombok.ToString;

@ToString
public class LoanExtendDto {

  public LoanExtendDto() {
  }

  public LoanExtendDto(long id) {
    this.id = id;
  }

  public long id;
}
