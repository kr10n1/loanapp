package com.quickloan.app.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

@ToString
public class LoanApplyDto {

  public LoanApplyDto() {
  }

  public LoanApplyDto(int term, int principal) {
    this.term = term;
    this.principal = principal;
  }

  @JsonProperty("term")
  public int term;
  @JsonProperty("principal")
  public int principal;
}
