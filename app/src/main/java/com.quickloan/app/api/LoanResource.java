package com.quickloan.app.api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.quickloan.app.service.ApplicationFacade;
import com.quickloan.app.service.ExtensionFacade;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log
@RestController
@RequestMapping("/loan")
public class LoanResource {

  private final ApplicationFacade applicationFacade;
  private final ExtensionFacade extensionFacade;

  public LoanResource(ApplicationFacade applicationFacade,
      ExtensionFacade extensionFacade) {
    this.applicationFacade = applicationFacade;
    this.extensionFacade = extensionFacade;
  }

  @RequestMapping(value = "/apply_for_loan"
      , method = POST
      , consumes = APPLICATION_JSON
      , produces = APPLICATION_JSON)
  public LoanApplicationResult applyForLoan(@RequestBody LoanApplyDto dto) {
    log.info(dto.toString());
    return applicationFacade.applyForLoan(dto);
  }

  @RequestMapping(value ="/extend_loan"
      , method = POST
      , consumes = APPLICATION_JSON
      , produces = APPLICATION_JSON)
  public LoanExtendResult extendLoan(@RequestBody LoanExtendDto dto) {
    log.info(dto.toString());
    return extensionFacade.extendLoan(dto);
  }
}
