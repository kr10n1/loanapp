package com.quickloan.app.service;

import com.quickloan.app.api.LoanExtendDto;
import com.quickloan.app.api.LoanExtendResult;
import org.springframework.stereotype.Component;

@Component
public class ExtensionFacade {

  private final LoanFacade loanFacade;

  public ExtensionFacade(LoanFacade loanFacade) {
    this.loanFacade = loanFacade;
  }

  public LoanExtendResult extendLoan(LoanExtendDto dto) {
    return loanFacade.extendLoan(dto);
  }
}
