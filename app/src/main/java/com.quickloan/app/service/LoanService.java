package com.quickloan.app.service;

import com.quickloan.app.configuration.Properties;
import com.quickloan.app.model.Loan;
import com.quickloan.app.repository.LoanRepository;
import com.quickloan.app.repository.NotFoundLoanException;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.springframework.stereotype.Component;

@Component
class LoanService {

  private final LoanRepository repository;
  private final RepaymentService repaymentService;
  private final int extensionTerm;
  private final int termInterest;

  LoanService(LoanRepository repository,
      RepaymentService repaymentService, Properties properties) {
    this.repository = repository;
    this.repaymentService = repaymentService;
    this.extensionTerm = properties.getExtensionTerm();
    this.termInterest = properties.getTermInterest();
  }

  Loan createLoan(int term, int amount) {
    LocalDate dueDate = repaymentService.calculateDueDate(term);
    BigDecimal repaymentAmount = repaymentService.calculateAmount(amount, termInterest);
    Loan loan = new Loan(term, amount, termInterest, repaymentAmount, dueDate);

    return repository.save(loan);
  }

  Loan extendLoan(long id) {
    return repository.findById(id)
        .map(this::extendTerm)
        .orElseThrow(NotFoundLoanException::new);

  }

  private Loan extendTerm(Loan loan) {
    loan.setTerm(loan.getTerm() + extensionTerm);
    loan.setOriginalDueDate(loan.getDueDate());
    loan.setDueDate(repaymentService.calculateExtendedDueDate(loan, extensionTerm));
    return repository.save(loan);
  }
}
