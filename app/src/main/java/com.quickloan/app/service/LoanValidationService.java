package com.quickloan.app.service;

import com.quickloan.app.api.LoanApplyDto;
import com.quickloan.app.api.LoanExtendDto;
import com.quickloan.app.service.validation.LoanExists;
import com.quickloan.app.service.validation.ValidAfterHours;
import com.quickloan.app.service.validation.ValidPrincipal;
import com.quickloan.app.service.validation.ValidTerm;
import org.springframework.stereotype.Component;

@Component
class LoanValidationService {

  private final ValidAfterHours validAfterHours;
  private final ValidPrincipal validPrincipal;
  private final ValidTerm validTerm;
  private final LoanExists loanExists;

  public LoanValidationService(
      ValidAfterHours validAfterHours,
      ValidPrincipal validPrincipal,
      ValidTerm validTerm, LoanExists loanExists) {
    this.validAfterHours = validAfterHours;
    this.validPrincipal = validPrincipal;
    this.validTerm = validTerm;
    this.loanExists = loanExists;
  }

  boolean isValidInput(LoanApplyDto dto) {
    return validTerm.isValid(dto) && validPrincipal.isValid(dto) && validAfterHours.isValid(dto);
  }

  boolean loanExists(LoanExtendDto dto) {
    return loanExists.isValid(dto);
  }
}
