package com.quickloan.app.service;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;

import com.quickloan.app.model.Loan;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.springframework.stereotype.Component;

@Component
class RepaymentService {
  private static final int PRECISION = 2;

  private final TimeService timeService;

  RepaymentService(TimeService timeService) {
    this.timeService = timeService;
  }

  BigDecimal calculateAmount(int loanPrincipal, int termInterest) {
    BigDecimal principal = valueOf(loanPrincipal);
    BigDecimal interest = valueOf(termInterest).movePointLeft(PRECISION);
    BigDecimal interestValue = principal.multiply(interest);
    BigDecimal amountToPay = principal.add(interestValue);
    return amountToPay.setScale(PRECISION, HALF_UP);
  }

  LocalDate calculateDueDate(int term) {
    return timeService.now().plusDays(term).toLocalDate();
  }

  public LocalDate calculateExtendedDueDate(Loan loan, int term) {
    return loan.getDueDate().plusDays(term);
  }
}
