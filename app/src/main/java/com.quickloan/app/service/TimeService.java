package com.quickloan.app.service;

import java.time.LocalDateTime;
import org.springframework.stereotype.Component;

@Component
public class TimeService {

  public LocalDateTime now() {
    return LocalDateTime.now();
  }
}
