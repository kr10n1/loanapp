package com.quickloan.app.service;

import static com.quickloan.app.api.LoanApplicationResult.rejected;

import com.quickloan.app.api.LoanApplicationResult;
import com.quickloan.app.api.LoanApplyDto;
import com.quickloan.app.model.Application;
import org.springframework.stereotype.Component;

@Component
public class ApplicationFacade {

  private final ApplicationService applicationService;
  private final LoanFacade loanFacade;
  private final LoanValidationService validationService;

  public ApplicationFacade(ApplicationService applicationService,
      LoanFacade loanFacade,
      LoanValidationService validationService) {
    this.applicationService = applicationService;
    this.loanFacade = loanFacade;
    this.validationService = validationService;
  }

  public LoanApplicationResult applyForLoan(LoanApplyDto dto) {
    Application app = applicationService.saveApplication(dto.principal, dto.term);
    LoanApplicationResult loanApplicationResult = rejected();
    if (validationService.isValidInput(dto)) {
      applicationService.acceptApplication(app);
      loanApplicationResult = loanFacade
          .createLoan(dto.term, dto.principal);
    }
    return loanApplicationResult;
  }
}
