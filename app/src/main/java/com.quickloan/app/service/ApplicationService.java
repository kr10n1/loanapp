package com.quickloan.app.service;

import static com.quickloan.app.model.Application.State.ACCEPTED;

import com.quickloan.app.model.Application;
import com.quickloan.app.repository.ApplicationRepository;
import org.springframework.stereotype.Component;

@Component
public class ApplicationService {

  private final ApplicationRepository applicationRepository;

  public ApplicationService(
      ApplicationRepository applicationRepository) {
    this.applicationRepository = applicationRepository;
  }

  Application saveApplication(int term, int principal) {
    return applicationRepository.save(new Application(principal, term));
  }

  Application acceptApplication(Application app) {
    app.setState(ACCEPTED);
    return applicationRepository.save(app);
  }
}
