package com.quickloan.app.service.validation;

import com.quickloan.app.api.LoanApplyDto;
import com.quickloan.app.configuration.Properties;
import org.springframework.stereotype.Component;

@Component
public class ValidPrincipal implements ApplyValidationPredicate {

  private final int minPrincipal;
  private final int maxPrincipal;

  public ValidPrincipal(Properties properties) {
    this.minPrincipal = properties.getMinAmount();
    this.maxPrincipal = properties.getMaxAmount();
  }

  @Override
  public boolean isValid(LoanApplyDto toValidate) {
    return minPrincipal < toValidate.principal && toValidate.principal <= maxPrincipal;
  }
}
