package com.quickloan.app.service.validation;

import com.quickloan.app.api.LoanExtendDto;

public interface ExtendValidationPredicate {

  boolean isValid(LoanExtendDto dto);
}
