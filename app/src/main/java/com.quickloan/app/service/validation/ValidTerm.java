package com.quickloan.app.service.validation;

import com.quickloan.app.api.LoanApplyDto;
import com.quickloan.app.configuration.Properties;
import org.springframework.stereotype.Component;

@Component
public class ValidTerm implements ApplyValidationPredicate {

  private final int minTerm;
  private final int maxTerm;

  public ValidTerm(Properties properties) {
    this.minTerm = properties.getMinTerm();
    this.maxTerm = properties.getMaxTerm();
  }

  @Override
  public boolean isValid(LoanApplyDto toValidate) {
    return minTerm < toValidate.term && toValidate.term <= maxTerm;
  }
}
