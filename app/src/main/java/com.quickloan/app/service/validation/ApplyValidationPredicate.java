package com.quickloan.app.service.validation;

import com.quickloan.app.api.LoanApplyDto;

public interface ApplyValidationPredicate {

  boolean isValid(LoanApplyDto toValidate);
}
