package com.quickloan.app.service.validation;

import com.quickloan.app.api.LoanApplyDto;
import com.quickloan.app.configuration.Properties;
import com.quickloan.app.service.TimeService;
import java.time.LocalTime;
import org.springframework.stereotype.Component;

@Component
public class ValidAfterHours implements ApplyValidationPredicate {

  private final TimeService timeService;
  private final int minPrincipal;
  private final int maxPrincipal;
  private final LocalTime openTime;
  private final LocalTime closeTime;

  public ValidAfterHours(TimeService timeService, Properties properties) {
    this.timeService = timeService;
    this.minPrincipal = properties.getMinAmount();
    this.maxPrincipal = properties.getMaxAmount();
    openTime = properties.getOpenTime();
    closeTime = properties.getCloseTime();
  }

  @Override
  public boolean isValid(LoanApplyDto toValidate) {
    if (afterHours()) {
      return toValidate.principal < maxPrincipal;
    } else {
      return true;
    }
  }

  private boolean afterHours() {
    LocalTime now = timeService.now().toLocalTime();
    return now.isBefore(openTime) & now.isAfter(closeTime);
  }
}
