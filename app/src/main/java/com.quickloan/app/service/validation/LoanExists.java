package com.quickloan.app.service.validation;

import com.quickloan.app.api.LoanExtendDto;
import com.quickloan.app.repository.LoanRepository;
import org.springframework.stereotype.Component;

@Component
public class LoanExists implements ExtendValidationPredicate {

  private final LoanRepository loanRepository;

  public LoanExists(LoanRepository loanRepository) {
    this.loanRepository = loanRepository;
  }

  @Override
  public boolean isValid(LoanExtendDto dto) {
    return loanRepository.findById(dto.id).isPresent();
  }
}
