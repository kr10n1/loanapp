package com.quickloan.app.service;

import static com.quickloan.app.api.LoanApplicationResult.success;
import static com.quickloan.app.api.LoanExtendResult.failed;
import static com.quickloan.app.api.LoanExtendResult.success;

import com.quickloan.app.api.LoanApplicationResult;
import com.quickloan.app.api.LoanExtendDto;
import com.quickloan.app.api.LoanExtendResult;
import com.quickloan.app.model.Loan;
import org.springframework.stereotype.Component;

@Component
public class LoanFacade {

  private final LoanValidationService loanValidationService;
  private final LoanService loanService;

  public LoanFacade(LoanValidationService loanValidationService, LoanService loanService) {
    this.loanValidationService = loanValidationService;
    this.loanService = loanService;
  }

  public LoanApplicationResult createLoan(int term, int amount) {
    Loan loan = loanService.createLoan(term, amount);
    return success(loan.getRepaymentAmount(), loan.getDueDate(), loan.getId());
  }

  public LoanExtendResult extendLoan(LoanExtendDto dto) {
    if (loanValidationService.loanExists(dto)) {
      loanService.extendLoan(dto.id);
      return success();
    }
    return failed("Loan does not exist");
  }
}
