package com.quickloan.app.configuration;

import java.time.LocalTime;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@ConfigurationProperties(prefix = "loan")
@PropertySource("classpath:application.yml")
public class Properties {

  private int minTerm;
  private int minAmount;
  private int maxTerm;
  private int maxAmount;
  private int termInterest;
  private int extensionTerm;
  private LocalTime openTime;
  private LocalTime closeTime;

  public void setOpenTime(String openTime) {
    this.openTime = LocalTime.parse(openTime);
  }

  public void setCloseTime(String closeTime) {
    this.closeTime = LocalTime.parse(closeTime);

  }
}


