package com.quickloan.app.model;

import static com.quickloan.app.model.Loan.State.OPEN;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.ToString;

@Entity
@ToString
public class Loan {


  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;

  @Basic(optional = false)
  private LocalDateTime created;

  private int principal;
  private int termInterest;
  private int term;
  private BigDecimal repaymentAmount;
  private LocalDate dueDate;
  private LocalDate originalDueDate;
  private State state;

  public Loan(int term, int principal, int termInterest, BigDecimal repaymentAmount,
      LocalDate dueDate) {
    this.term = term;
    this.principal = principal;
    this.termInterest = termInterest;
    this.repaymentAmount = repaymentAmount;
    this.dueDate = dueDate;
    this.created = LocalDateTime.now();
    this.state = OPEN;
  }

  public int getTerm() {
    return term;
  }

  public int getPrincipal() {
    return principal;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public int getTermInterest() {
    return termInterest;
  }

  public void setTerm(int term) {
    this.term = term;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public LocalDate getDueDate() {
    return dueDate;
  }

  public void setDueDate(LocalDate dueDate) {
    this.dueDate = dueDate;
  }

  public BigDecimal getRepaymentAmount() {
    return repaymentAmount;
  }

  public LocalDate getOriginalDueDate() {
    return originalDueDate;
  }

  public void setOriginalDueDate(LocalDate originalDueDate) {
    this.originalDueDate = originalDueDate;
  }


  public enum State {
    OPEN,
    PAID
  }
}
