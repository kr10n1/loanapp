package com.quickloan.app.model;

import static com.quickloan.app.model.Application.State.REJECTED;
import static javax.persistence.GenerationType.AUTO;

import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.ToString;

@Entity
@ToString
public class Application {

  @Id
  @GeneratedValue(strategy = AUTO)
  @Column(name = "ID")
  private Long id;
  @Basic(optional = false)
  private LocalDateTime created;
  private int principal;
  private int term;
  @OneToOne
  private Loan loan;
  private State state;

  private Application() {
  }

  public Application(int principal, int term) {
    this.principal = principal;
    this.term = term;
    this.created = LocalDateTime.now();
    this.state = REJECTED;
  }

  public Long getId() {
    return id;
  }

  public int getPrincipal() {
    return principal;
  }

  public void setPrincipal(int principal) {
    this.principal = principal;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public int getTerm() {
    return term;
  }

  public void setTerm(int term) {
    this.term = term;
  }

  public Loan getLoan() {
    return loan;
  }

  public void setLoan(Loan loan) {
    this.loan = loan;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }


  public enum State {
    ACCEPTED,
    REJECTED
  }
}
