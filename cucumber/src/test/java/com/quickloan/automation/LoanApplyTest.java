package com.quickloan.automation;

import static cucumber.api.SnippetType.CAMELCASE;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    glue = "com.quickloan.automation.steps",
    plugin = {"html:target/selenium-reports", "pretty"},
    features = "src/test/resources/loan.feature",
    snippets = CAMELCASE
)
public class LoanApplyTest {

}
