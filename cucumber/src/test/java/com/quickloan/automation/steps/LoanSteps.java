package com.quickloan.automation.steps;

import static com.quickloan.automation.steps.LoanApplicationResult.Status.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.Gson;
import java.net.URI;
import lombok.extern.java.Log;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.util.UriComponentsBuilder;

@Log
public class LoanSteps {

  private static final String URL = "http://localhost:8080/loan/apply_for_loan";
  private int principal;
  private int term;

  private CloseableHttpClient httpClient = HttpClients.createDefault();
  private CloseableHttpResponse response;
  private LoanApplicationResult loanApplicationResult;

  public LoanSteps() {}



  @Given("^I am customer applying for loan with (\\d+) amount and (\\d+) term$")
  public void iAmCustomerApplyingForLoanWithAmountAndTerm(int arg0, int arg1) {
    principal = arg0;
    term = arg1;
  }

  @When("^I send request to apply_for_loan url$")
  public void iSendRequestToApply_for_loanUrl() throws Throwable {
    URI apply_for_loan = UriComponentsBuilder.fromHttpUrl(URL)
        .build()
        .toUri();
    HttpPost request = new HttpPost(apply_for_loan);
    Gson gson = new Gson();
    String json = gson.toJson(new Request(principal, term));
    StringEntity stringEntity = new StringEntity(json);
    request.setEntity(stringEntity);
    request.setHeader("Content-Type", "application/json");
    response = httpClient.execute(request);

    String jsonResponse = EntityUtils.toString(response.getEntity());
    loanApplicationResult = gson
        .fromJson(jsonResponse, LoanApplicationResult.class);
  }

  @Then("^I receive SUCCESS message$")
  public void iReceiveSUCCESSMessage() {
    assertEquals(200, response.getStatusLine().getStatusCode());
    assertEquals(loanApplicationResult.status, SUCCESS);

  }

  @And("^application id$")
  public void applicationId() {
    assertNotNull(loanApplicationResult.applicationId);
  }

  @And("repaymentAmount {int}")
  public void repaymentamount(int repaymentAmount) {
    assertNotNull(loanApplicationResult.repaymentAmount);
    assertEquals(repaymentAmount, loanApplicationResult.repaymentAmount.intValue());
  }

  @And("^repaymentDate$")
  public void repaymentdate() {
    assertNotNull(loanApplicationResult.repaymentDate);
  }
}
