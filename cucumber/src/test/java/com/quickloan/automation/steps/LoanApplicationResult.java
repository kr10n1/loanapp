package com.quickloan.automation.steps;

import java.math.BigDecimal;

public class LoanApplicationResult {

  public final BigDecimal repaymentAmount;
  public final String repaymentDate;
  public final Long applicationId;
  public final Status status;

  private LoanApplicationResult(BigDecimal repaymentAmount, String repaymentDate,
      Long applicationId,
      Status status) {
    this.repaymentAmount = repaymentAmount;
    this.repaymentDate = repaymentDate;
    this.applicationId = applicationId;
    this.status = status;
  }

  public enum Status {
    REJECTED,
    SUCCESS
  }
}
