Feature: New loan process

  Scenario: apply for loan
    Given I am customer applying for loan with 500 amount and 12 term
    When I send request to apply_for_loan url
    Then I receive SUCCESS message
    And application id
    And repaymentAmount 550
    And repaymentDate
