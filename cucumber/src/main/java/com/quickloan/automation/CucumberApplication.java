package com.quickloan.automation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CucumberApplication implements CommandLineRunner {

  public static void main(String[] args) {
    SpringApplication application = new SpringApplication(CucumberApplication.class);
    application.run(args);
  }

  @Override
  public void run(final String... strings) throws Exception {

  }
}